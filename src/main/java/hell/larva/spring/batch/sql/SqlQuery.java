package hell.larva.spring.batch.sql;

public class SqlQuery {
    public final static String PRODUCER_LOAD_ALL = "SELECT * FROM producer";
    public final static String PRODUCT_LOAD_ALL = "SELECT * FROM PRODUCT";
    public final static String UPDATE_PRODUCER = "INSERT INTO Producer(seq_no, producer_name, sys_status) VALUES (?,?,?)";
}
