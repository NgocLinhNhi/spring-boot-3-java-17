package hell.larva.spring.batch.constant;

public class JasperReportConstant {

    public static final String IS_DISPLAY_HEADER = "IS_DISPLAY_HEADER";
    public static final String IS_PDF = "IS_PDF";
    public static final String IS_CSV = "IS_CSV";

    public static final int BLOCK_SIZE = 4096;
    public static final int MIN_GROW_COUNT = 2048;

    public interface EXPORT_FILE_TYPE {
        int CSV = 1;
        int TXT = 2;
        int PDF = 3;
        int XLS = 4;
    }

    public interface REPORT_TEMPLATE_CD {
        String D_RPT_PRODUCT = "RPT001_D_PRODUCT";
        String M_RPT_PRODUCT = "RPT002_D_PRODUCT";
    }

    public interface FOLDER_REPORT_CONFIG {
        String D_RPT_PRODUCT = "rpt001-daily-producer";
        String M_RPT_PRODUCT = "rpt002-daily-producer";
    }

    public interface SUMMARY_TYPE {
        String INIT_TASKLET = "BAT000_INIT_TASKLET";
        String UPDATE_PRODUCER_TASKLET = "BAT002_UPDATE_PRODUCER";
    }

}
