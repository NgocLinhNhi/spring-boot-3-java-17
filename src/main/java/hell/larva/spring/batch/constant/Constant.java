package hell.larva.spring.batch.constant;

public class Constant {
    public static final String DATE = "REPORT DATE";
    public static final String SEQ = "SEQ";
    public static final String PRODUCER_NAME = "PRODUCER_NAME";
    public static final String STATUS = "STATUS";

    public static final String ACTIVE = "ACTIVE";
    public static final String UN_ACTIVE = "UN-ACTIVE";

}
