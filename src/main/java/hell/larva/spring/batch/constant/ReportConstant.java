package hell.larva.spring.batch.constant;

public interface ReportConstant {

    interface ENCODING {
        String UTF8 = "UTF-8";
    }

    interface FILE_EXTENSION {
        String JPG = ".jpg";
        String BMP = ".bmp";
        String PDF = ".pdf";
        String TXT = ".txt";
        String CSV = ".csv";
        String XLS = ".xls";
        String XLXS = ".xlsx";
        String JASPER_EXTENSION = ".jasper";
        String JRXML_EXTENSION = ".jrxml";
    }
}
