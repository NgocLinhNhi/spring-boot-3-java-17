package hell.larva.spring.batch.interfaces;

import hell.larva.spring.batch.entity.Producer;
import hell.larva.spring.batch.entity.Product;

import java.util.List;

public interface IProductDao {

    List<Producer> loadAllProducer();

    List<Product> loadAllProduct();
}
