package hell.larva.spring.batch.utils.jasper;

import hell.larva.spring.batch.constant.ReportConstant;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.util.AbstractSampleApp;
import net.sf.jasperreports.export.*;
import org.apache.log4j.Logger;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.util.Map;

public class JasperExportFactory extends AbstractSampleApp implements IJasperFactory {
    private static final Logger logger = Logger.getLogger(JasperExportFactory.class);

    private Connection conn;
    private String outputFile;
    private Map params;
    private JasperReport jasperReportType;
    private boolean checkSwapFile;

    private static JasperExportFactory INSTANCE;

    private JasperExportFactory() {
    }

    public void init(Connection conn,
                     String outputFile,
                     Map params,
                     JasperReport jasperReportType,
                     boolean checkSwapFile) {
        this.conn = conn;
        this.outputFile = outputFile;
        this.params = params;
        this.jasperReportType = jasperReportType;
        this.checkSwapFile = checkSwapFile;
    }

    public static JasperExportFactory getInstance() {
        if (INSTANCE == null) {
            logger.info("Created new Singleton JasperExportFactory object");
            INSTANCE = new JasperExportFactory();
        }
        return new JasperExportFactory();
    }

    @Override
    public boolean exportToPDF() {
        File swapFile = null;
        try {
            if (checkSwapFile) swapFile = JasperReportUtils.createSwapFolder(outputFile, params);
            JasperPrint jasperPrint = JasperReportUtils.checkUseSql(params, jasperReportType, conn);
            exportPdfReport(jasperPrint);
        } catch (Exception e) {
            logger.error("Export to PDF has failed: ", e);
            return false;
        } finally {
            JasperReportUtils.deleteSwapFile(swapFile);
        }
        return true;
    }

    @Override
    public boolean exportToCSV() {
        File swapFile = null;
        try {
            File file = new File(outputFile + ReportConstant.FILE_EXTENSION.CSV);
            try (FileOutputStream outStream = new FileOutputStream(file);
                 ByteArrayOutputStream outputByteArray = new ByteArrayOutputStream()) {
                //byte-order marker (BOM)
                JasperReportUtils.writeBom(outStream);

                if (checkSwapFile) swapFile = JasperReportUtils.createSwapFolder(outputFile, params);
                JasperPrint jasperPrint = createJasperPrint();
                exportCsv(outStream, outputByteArray, jasperPrint);
            }
        } catch (Exception e) {
            logger.error("Export to CSV has failed: ", e);
            return false;
        } finally {
            JasperReportUtils.deleteSwapFile(swapFile);
        }
        return true;
    }

    @Override
    public boolean exportToTXT() {
        try {
            JasperPrint jasperPrint = createJasperPrint();
            exportTXT(jasperPrint);
        } catch (Exception e) {
            logger.error("Export to TXT has failed: ", e);
            return false;
        }
        return true;
    }

    @Override
    public boolean exportToXLS() {
        try {
            JasperPrint jasperPrint = JasperReportUtils.checkUseSql(params, jasperReportType, conn);
            exportExcel(jasperPrint);
        } catch (Exception e) {
            logger.error("Export to XLS has failed: ", e);
            return false;
        }
        return true;
    }

    private void exportPdfReport(JasperPrint jasperPrint) throws JRException {
        JRPdfExporter exporter = new JRPdfExporter();
        exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
        exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(outputFile + ReportConstant.FILE_EXTENSION.PDF));
        SimplePdfExporterConfiguration configuration = new SimplePdfExporterConfiguration();
        exporter.setConfiguration(configuration);
        exporter.exportReport();
    }

    private void exportCsv(FileOutputStream outStream,
                           ByteArrayOutputStream outputByteArray,
                           JasperPrint jasperPrint) throws JRException, IOException {
        JRCsvExporter exporter = new JRCsvExporter();
        exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
        exporter.setExporterOutput(new SimpleWriterExporterOutput(outputByteArray, ReportConstant.ENCODING.UTF8));
        exporter.exportReport();

        outStream.write(outputByteArray.toByteArray());
        flush(outStream, outputByteArray);
    }

    private void exportTXT(JasperPrint jasperPrint) throws JRException {
        JRCsvExporter exporter = new JRCsvExporter();
        exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
        exporter.setExporterOutput(new SimpleWriterExporterOutput(outputFile + ReportConstant.FILE_EXTENSION.TXT));
        exporter.exportReport();
    }

    private void exportExcel(JasperPrint jasperPrint) throws JRException {
        JRXlsExporter exporter = new JRXlsExporter();
        exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
        exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(outputFile + ReportConstant.FILE_EXTENSION.XLS));
        exporter.setConfiguration(createSimpleXlsReportConfiguration());
        exporter.exportReport();
    }

    private JasperPrint createJasperPrint() throws JRException {
        JasperPrint jasperPrint = JasperReportUtils.checkUseSql(params, jasperReportType, conn);
        jasperPrint.getPropertiesMap().setProperty("net.sf.jasperreports.export.csv.exclude.origin.band.pageFooter", "pageFooter");
        return jasperPrint;
    }

    private void flush(FileOutputStream outStream,
                       ByteArrayOutputStream outputByteArray) throws IOException {
        outStream.flush();
        outputByteArray.flush();
    }

    private SimpleXlsReportConfiguration createSimpleXlsReportConfiguration() {
        SimpleXlsReportConfiguration configuration = new SimpleXlsReportConfiguration();
        configuration.setOnePagePerSheet(false);
        configuration.setRemoveEmptySpaceBetweenRows(false);
        configuration.setDetectCellType(false);
        configuration.setWhitePageBackground(false);
        configuration.setIgnoreGraphics(true);
        configuration.setRemoveEmptySpaceBetweenColumns(true);
        configuration.setRemoveEmptySpaceBetweenRows(true);
        configuration.setCollapseRowSpan(true);
        return configuration;
    }

    @Override
    public void test() {
    }

}
