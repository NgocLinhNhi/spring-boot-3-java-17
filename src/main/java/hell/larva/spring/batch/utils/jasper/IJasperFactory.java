package hell.larva.spring.batch.utils.jasper;

public interface IJasperFactory {
    boolean exportToPDF();

    boolean exportToCSV();

    boolean exportToTXT();

    boolean exportToXLS();
}
