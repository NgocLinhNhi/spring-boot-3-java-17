package hell.larva.spring.batch.utils.csv;

public class CsvWriteException extends RuntimeException {

    public CsvWriteException(String msg, Exception e) {
        super(msg, e);
    }

}
