package hell.larva.spring.batch.utils.jasper;

import hell.larva.spring.batch.constant.JasperReportConstant;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.fill.JRSwapFileVirtualizer;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.util.JRSwapFile;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.util.Map;
import java.util.Objects;

import static hell.larva.spring.batch.constant.ReportConstant.FILE_EXTENSION.JASPER_EXTENSION;
import static hell.larva.spring.batch.constant.ReportConstant.FILE_EXTENSION.JRXML_EXTENSION;

@SuppressWarnings("rawtypes")
public class JasperReportUtils {

    private static final Logger logger = Logger.getLogger(JasperExportFactory.class);

    @SuppressWarnings("unchecked")
    //Tạo swap file => quy định mỗi file export với mỗi thread chỉ được chiếm dụng bao nhiêu Ram => không bị out of memory
    //VỚi trường hợp 16 thread => export 16 file large data => gây ra out of memory
    static File createSwapFolder(String outputFile, Map params) {
        File swapFolder = createOutputFolder(outputFile);
        JRSwapFile swapFile = new JRSwapFile(
                outputFile,
                JasperReportConstant.BLOCK_SIZE,
                JasperReportConstant.MIN_GROW_COUNT);
        JRSwapFileVirtualizer virtualized = new JRSwapFileVirtualizer(100, swapFile, true);
        params.put(JRParameter.REPORT_VIRTUALIZER, virtualized);
        return swapFolder;
    }

    static void deleteSwapFile(File swapFile) {
        try {
            if (swapFile != null) delete(swapFile);
        } catch (Exception ex) {
            logger.error("Has error occurred in handling delete swap file", ex);
        }
    }

    private static void delete(File file) {
        //Chỉ có thể delete old file swap -> file swap mới tạo ra không delete được => Cũng OK = quét file.listFiles
        //Lý do trước code không thể xóa được file swap chỉ với file.delete()
        logger.info("File path need to delete: " + file);
        for (File subFile : Objects.requireNonNull(file.listFiles())) {
            if (subFile.isDirectory()) {
                logger.info("Is Directory : " + subFile);
                delete(subFile);
            } else {
                logger.info("File swap need to delete:" + subFile);
                boolean subFileResult = subFile.delete();
                if (subFileResult) logger.info("Delete swap file has done !!!");
                else logger.info("This file swap is reading !!!");
            }
        }
    }

    public static File createOutputFolder(String outputFolder) {
        File outDir = new File(outputFolder);
        if (!outDir.exists()) {
            boolean mkdirs = outDir.mkdirs();
            if (mkdirs) logger.info("create output folder on server has successfully !!!");
            else logger.info("Folder has existed on server");
        }
        return outDir;
    }

    //For C2 : load file template .jrxml từ nội project
    public static String getTemplatePath(String filePath, String fileName) {
        String result = JasperReportUtils.getFilePath(filePath, fileName);
        return result.replace(JRXML_EXTENSION, "");
    }

    private static String getFilePath(String filePath, String fileName) {
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        return Objects.requireNonNull(loader.getResource(filePath + fileName)).getPath();
    }

    //C1 : Load file template .jrxml from 1 folder config on server
    private static File generateFileJrxmlToJasper(String templateFile) throws JRException {
        File file = new File(templateFile + JASPER_EXTENSION);
        if (!file.exists()) {
            JasperCompileManager.compileReportToFile(
                    templateFile + JRXML_EXTENSION,
                    templateFile + JASPER_EXTENSION);
            file = new File(templateFile + JASPER_EXTENSION);
        }
        return file;
    }

    public static JasperReport generateJasperTemplate(String templateFile) throws JRException {
        File file = JasperReportUtils.generateFileJrxmlToJasper(templateFile);
        return (JasperReport) JRLoader.loadObject(file);
    }

    static void writeBom(FileOutputStream outStream) throws IOException {
        logger.info("Write Bom byte");
        byte[] b = {(byte) 0xEF, (byte) 0xBB, (byte) 0xBF};
        outStream.write(b);
    }

    @SuppressWarnings("unchecked")
    static JasperPrint checkUseSql(Map params,
                                   JasperReport jasperReportPDF,
                                   Connection conn) throws JRException {
        //check file jasper report có dùng sql nội trong file pdf hay không - có thì truyền connection vào
        if (conn == null) return JasperFillManager.fillReport(jasperReportPDF, params);
        else return JasperFillManager.fillReport(jasperReportPDF, params, conn);
    }
}
