package hell.larva.spring.batch.tasklet;

import hell.larva.spring.batch.bean.BatchConfiguration;
import hell.larva.spring.batch.config.CommonMemory;
import hell.larva.spring.batch.constant.JasperReportConstant;
import hell.larva.spring.batch.dao.ProductDaoImpl;
import hell.larva.spring.batch.entity.Producer;
import hell.larva.spring.batch.service.ProductService;
import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.util.Assert;

import java.util.List;

@Getter
@Setter
public class ProducerTasklet implements Tasklet, InitializingBean {

    private SettingTasklet settingTasklet;
    private ProductDaoImpl productDao;

    private final Logger logger = LoggerFactory.getLogger(ProducerTasklet.class);

    @Override
    public RepeatStatus execute(StepContribution arg0, ChunkContext arg1) {
        try {
            logger.info("========== START RUN PRODUCER TASKLET ==========");

            getDateByCommonClass();
            String reportDate = settingTasklet.getReportDate();
            if (reportDate == null) throw new IllegalArgumentException("Report Date is null");
            ProductService productService = getProductService();
            List<Producer> listProducer = productService.getListProducer();

            //Java 8 default method - xào nấu listProducer không cần trả về
            productService.awake(listProducer);

            productService.exportToFile(listProducer, reportDate, settingTasklet.getDataDailyExportFilePath());
            logger.info("========== FINISH RUN PRODUCER TASKLET ==========");
            CommonMemory.getInstance().updateBatchStatus(
                    JasperReportConstant.SUMMARY_TYPE.UPDATE_PRODUCER_TASKLET,
                    true,
                    null);
        } catch (Exception e) {
            CommonMemory.getInstance().updateBatchStatus(
                    JasperReportConstant.SUMMARY_TYPE.UPDATE_PRODUCER_TASKLET,
                    false,
                    e);
            throw e;
        }

        return RepeatStatus.FINISHED;
    }

    private ProductService getProductService() {
        return new ProductService(productDao);
    }

    @Override
    public void afterPropertiesSet() {
        Assert.notNull(settingTasklet.getDataDailyExportFilePath(), "dataDailyExportFilePath must be set");
    }

    private void getDateByCommonClass() {
        String reportDate = BatchConfiguration.getBatchConfiguration().getReportDate();
        System.out.println("Report Date from Common class : " + reportDate);
    }

}
