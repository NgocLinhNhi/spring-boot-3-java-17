package hell.larva.spring.batch.tasklet.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReportConfiguration {

    private String dbThreadPoolSize;
    private String exportFilepath;
    private String templateFilePath;

    private static ReportConfiguration instance;

    public static synchronized ReportConfiguration getReportConfiguration() {
        if (instance == null) {
            instance = new ReportConfiguration();
        }
        return instance;
    }

    public int getThreadPoolSize() {
        return Integer.parseInt(dbThreadPoolSize);
    }

    public static void setInstance(ReportConfiguration instance) {
        ReportConfiguration.instance = instance;
    }

    public void setExportFilepath(String exportFilepath) {
        this.exportFilepath = exportFilepath.trim();
    }

    public void setTemplateFilePath(String templateFilePath) {
        this.templateFilePath = templateFilePath.trim();
    }
}
