package hell.larva.spring.batch.tasklet;

import hell.larva.spring.batch.bean.BatchConfiguration;
import hell.larva.spring.batch.config.CommonMemory;
import hell.larva.spring.batch.config.SpringApplicationContext;
import hell.larva.spring.batch.constant.JasperReportConstant;
import hell.larva.spring.batch.utils.date.DateUtil;
import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

@Getter
@Setter
public class SettingTasklet implements Tasklet {
    private String reportDate;
    private BatchConfiguration configuration;
    private String dataDailyExportFilePath;

    private final Logger logger = LoggerFactory.getLogger(ProducerTasklet.class);

    @Override
    public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) {
        try {
            logger.info("========== START INIT BATCH SETTINGS TASK ==========");
            //clear all result run batch before
            CommonMemory.clearMem();

            //C1 get DateFormat by Bean thông qua SpringApplicationContext
            getBeanBySpringApplicationContext();

            //C2: thông qua singleTon BatchConfiguration không cần bean
            String dateFormat = BatchConfiguration.getBatchConfiguration().getDateFormat();
            System.out.println("dateFormat singleton: " + dateFormat);

            //C3 : lấy = bean BatchConfiguration config trong xml
            if (configuration.getDateFormat() == null) {
                System.out.println("DATE FORMAT CAN NOT NULL");
                return null;
            }

            System.out.println("DATE FORMAT from BatchConfiguration Bean " + configuration.getDateFormat());
            this.reportDate = DateUtil.getCurrentNextDate(configuration.getDateFormat());
            setReportDate();
            System.out.println(this.reportDate);

            logger.info("========== END INIT BATCH SETTINGS TASK ==========");
            CommonMemory.getInstance().updateBatchStatus(JasperReportConstant.SUMMARY_TYPE.INIT_TASKLET, true, null);
        } catch (Exception e) {
            CommonMemory.getInstance().updateBatchStatus(JasperReportConstant.SUMMARY_TYPE.INIT_TASKLET, false, e);
            throw e;
        }
        return RepeatStatus.FINISHED;
    }

    private void getBeanBySpringApplicationContext() {
        BatchConfiguration configuration = (BatchConfiguration) SpringApplicationContext.getBean("batchConfiguration");
        System.out.println("Date Format from SpringApplicationContext : " + configuration.getDateFormat());
    }

    private void setReportDate() {
        String currentNextDate = DateUtil.getCurrentNextDate(configuration.getDateFormat());
        BatchConfiguration.getBatchConfiguration().setReportDate(currentNextDate);
    }
}
