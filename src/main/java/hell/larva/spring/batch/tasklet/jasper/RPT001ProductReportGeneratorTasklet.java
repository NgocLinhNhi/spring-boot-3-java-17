package hell.larva.spring.batch.tasklet.jasper;

import hell.larva.spring.batch.config.CommonMemory;
import hell.larva.spring.batch.constant.JasperReportConstant;
import hell.larva.spring.batch.dao.ProductDaoImpl;
import hell.larva.spring.batch.entity.Product;
import hell.larva.spring.batch.service.BatchRunResult;
import hell.larva.spring.batch.tasklet.SettingTasklet;
import hell.larva.spring.batch.tasklet.bean.ReportConfiguration;
import lombok.Getter;
import lombok.Setter;
import org.apache.log4j.Logger;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import javax.sql.DataSource;
import java.util.List;

@Getter
@Setter
public class RPT001ProductReportGeneratorTasklet implements Tasklet {

    private Logger logger = Logger.getLogger(RPT001ProductReportGeneratorTasklet.class);

    private SettingTasklet settingTasklet;
    private ProductDaoImpl productDao;
    private ReportConfiguration config;
    private DataSource dataSource;

    @Override
    public RepeatStatus execute(StepContribution arg0, ChunkContext arg1) throws Exception {
        try {
            logger.info("========== START GENERATION REPORT RPT - DAILY PRODUCT  ==========");

            String reportDate = settingTasklet.getReportDate();
            List<Product> products = productDao.loadAllProduct();
            logger.info("PRODUCT loaded: " + products.size());

            if (products.size() > 0) {
                RPT001ProductService rpt001ProductService = getRPT001ProductService();
                logger.info("Begin export PDF report: RPT_001_D for product " + products.size());
                rpt001ProductService.generateReport(JasperReportConstant.EXPORT_FILE_TYPE.PDF, reportDate);
                logger.info("Begin export CSV report: RPT_001_D for  product " + products.size());
                rpt001ProductService.generateReport(JasperReportConstant.EXPORT_FILE_TYPE.CSV, reportDate);
            }
            CommonMemory.getInstance().updateBatchStatus(
                    JasperReportConstant.REPORT_TEMPLATE_CD.D_RPT_PRODUCT,
                    true,
                    null);
            logger.info("========== FINISH GENERATION REPORT  - DAILY PRODUCT  ==========");

            // check all result run batch
            BatchRunResult.getInstance().checkRunBatchResult();
        } catch (Exception e) {
            CommonMemory.getInstance().updateBatchStatus(
                    JasperReportConstant.REPORT_TEMPLATE_CD.D_RPT_PRODUCT,
                    false, e);
            throw e;
        }
        return RepeatStatus.FINISHED;
    }

    private RPT001ProductService getRPT001ProductService() {
        return new RPT001ProductService(config, dataSource);
    }

}
