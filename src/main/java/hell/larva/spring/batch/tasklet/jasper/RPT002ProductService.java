package hell.larva.spring.batch.tasklet.jasper;

import hell.larva.spring.batch.entity.Product;
import hell.larva.spring.batch.tasklet.bean.ReportConfiguration;
import hell.larva.spring.batch.utils.jasper.JasperExportFactory;
import hell.larva.spring.batch.utils.jasper.JasperReportUtils;
import net.sf.jasperreports.engine.JasperReport;
import org.apache.log4j.Logger;

import javax.sql.DataSource;
import java.io.File;
import java.sql.Connection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static hell.larva.spring.batch.constant.JasperReportConstant.*;

class RPT002ProductService {
    private final Logger logger = Logger.getLogger(RPT002ProductService.class);
    private static final String REPORT_PREFIX = "RPT002-DAILY-PRODUCT-REPORT-";

    private final ReportConfiguration config;
    private final DataSource dataSource;
    private ExecutorService generator;

    RPT002ProductService(ReportConfiguration config, DataSource dataSource) {
        this.config = config;
        this.dataSource = dataSource;
    }

    private void createThreadPool() {
        //pool của jdbc connection phải > executor thread pool => không được =
        generator = Executors.newFixedThreadPool(config.getThreadPoolSize());
    }

    //1 kiểu Facade pattern
    void generateReport(List<Product> listProduct,
                        int reportFileType,
                        String reportDate) throws Exception {

        String outputFolder = createOutputFolderPath(reportDate);
        logger.info("Output folder path report-002 : " + outputFolder);
        JasperReportUtils.createOutputFolder(outputFolder);

        String templateFile = createTemplateFilePath(config.getTemplateFilePath());
        logger.info("Template file name report-002 : " + templateFile);

        JasperReport jasperReport = JasperReportUtils.generateJasperTemplate(templateFile);
        exportJasperReport(listProduct, reportFileType, jasperReport, reportDate, outputFolder);
    }


    private void exportJasperReport(List<Product> listProduct,
                                    int exportType,
                                    JasperReport jasperReport,
                                    String reportDate,
                                    String outputFolder) throws InterruptedException {
        createThreadPool();
        final CountDownLatch latch = new CountDownLatch(listProduct.size());

        for (Product product : listProduct) {
            Long seqPro = product.getSeqPro();
            final Map<String, Object> params = createParams(reportDate, product);
            String exportFullFileName = createExportFilePath(outputFolder, reportDate, seqPro);
            logger.info("Export Full file name report-002 : " + exportFullFileName);
            generator.execute(() -> exportReport(exportType, jasperReport, latch, params, exportFullFileName));
        }

        latch.await();
        generator.shutdown();
        boolean finished = generator.awaitTermination(1, TimeUnit.DAYS);
        if (finished) logger.info("Finish generation");
        else logger.error("Generate report take more than one day. Stop");
    }

    private void exportReport(int exportType,
                              JasperReport jasperReport,
                              CountDownLatch latch,
                              Map<String, Object> params,
                              String exportFullFileName) {

        try (Connection conn = dataSource.getConnection()) {
            //Khai báo biến Var : java 10
            var start = System.currentTimeMillis();
            var result = false;
            JasperExportFactory instance = JasperExportFactory.getInstance();

            //Switch expression java 12
            switch (exportType) {
                case EXPORT_FILE_TYPE.PDF -> {
                    params.put(IS_DISPLAY_HEADER, IS_PDF);
                    instance.init(conn,
                            exportFullFileName,
                            params,
                            jasperReport,
                            false);
                    result = instance.exportToPDF();
                }
                case EXPORT_FILE_TYPE.CSV -> {
                    params.put(IS_DISPLAY_HEADER, IS_CSV);
                    instance.init(conn,
                            exportFullFileName,
                            params,
                            jasperReport,
                            false);
                    result = instance.exportToCSV();
                }
                default -> logger.info("Invalid report Type");
            }

            long end = System.currentTimeMillis();
            if (result) {
                logger.info("Export jasper report succeed with time : " + (end - start) + "ms");
            } else {
                logger.error("Export jasper report has failed !!! ");
            }
            latch.countDown();
        } catch (Exception e) {
            logger.error("Generate report 002-Product has failed: ", e);
        }
    }


    private Map<String, Object> createParams(String reportDate, Product product) {
        final Map<String, Object> params = new HashMap<>();
        params.put("seqPro", String.valueOf(product.getSeqPro()));
        params.put("parameter1", "Ninh Ninh");
        params.put("parameter2", "Kute");
        params.put("parameter3", "1991");
        params.put("reportTitle", "PRODUCT-REPORT");
        params.put("reportDate", reportDate);
        params.put("fullName", product.getProductName());
        return params;
    }

    private String createTemplateFilePath(String templateFilePath) {
        return templateFilePath + REPORT_TEMPLATE_CD.M_RPT_PRODUCT;
    }

    private String createOutputFolderPath(String reportDate) {
        return config.getExportFilepath()
                + File.separator
                + FOLDER_REPORT_CONFIG.M_RPT_PRODUCT
                + File.separator
                + reportDate;
    }

    private String createExportFilePath(String outputFolder, String reportDate, Long seqPro) {
        return outputFolder + File.separator + makeFileName(reportDate, seqPro);
    }

    private String makeFileName(String reportDate, Long seqPro) {
        return REPORT_PREFIX + seqPro + "-" + reportDate;
    }

}
