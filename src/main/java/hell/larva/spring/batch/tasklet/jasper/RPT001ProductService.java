package hell.larva.spring.batch.tasklet.jasper;

import hell.larva.spring.batch.constant.JasperReportConstant;
import hell.larva.spring.batch.tasklet.bean.ReportConfiguration;
import hell.larva.spring.batch.utils.jasper.JasperExportFactory;
import hell.larva.spring.batch.utils.jasper.JasperReportUtils;
import net.sf.jasperreports.engine.JasperReport;
import org.apache.log4j.Logger;

import javax.sql.DataSource;
import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

class RPT001ProductService {
    private final Logger logger = Logger.getLogger(RPT001ProductService.class);
    private static final String REPORT_PREFIX = "RPT001-PRODUCT-REPORT-";
    private static final String ABSOLUTE_PATH = "templates/";
    private static final String JASPER_REPORT_FILE_NAME = "RPT001_D_PRODUCT.jrxml";

    private final ReportConfiguration config;
    private final DataSource dataSource;

    RPT001ProductService(ReportConfiguration config, DataSource dataSource) {
        this.config = config;
        this.dataSource = dataSource;
    }

    //1 kiểu Facade pattern
    void generateReport(int reportFileType,
                        String reportDate) throws Exception {

        String outputFolder = createOutputFilePath(reportDate);
        logger.info("Output folder path report-001 : " + outputFolder);
        JasperReportUtils.createOutputFolder(outputFolder);

        String templateFile = createTemplateFilePath(config.getTemplateFilePath());
        logger.info("Template file name : " + templateFile);

        JasperReport jasperReport = JasperReportUtils.generateJasperTemplate(templateFile);

        String exportFullFileName = createExportFilePath(outputFolder, reportDate);
        logger.info("Export Full file name rpt-001: " + exportFullFileName);

        Map<String, Object> params = createParams(reportDate);
        exportJasperReport(reportFileType, jasperReport, exportFullFileName, params);
    }

    private void exportJasperReport(int reportFileType,
                                    JasperReport jasperReport,
                                    String exportFullFileName,
                                    Map<String, Object> params) throws SQLException {
        try (Connection conn = dataSource.getConnection()) {
            long start = System.currentTimeMillis();
            boolean result = false;
            JasperExportFactory instance = JasperExportFactory.getInstance();

            //Switch expression java 12
            switch (reportFileType) {
                case JasperReportConstant.EXPORT_FILE_TYPE.PDF -> {
                    params.put(JasperReportConstant.IS_DISPLAY_HEADER, JasperReportConstant.IS_PDF);
                    instance.init(conn,
                            exportFullFileName,
                            params,
                            jasperReport,
                            true);
                    result = instance.exportToPDF();
                }
                case JasperReportConstant.EXPORT_FILE_TYPE.CSV -> {
                    params.put(JasperReportConstant.IS_DISPLAY_HEADER, JasperReportConstant.IS_CSV);
                    instance.init(conn,
                            exportFullFileName,
                            params,
                            jasperReport,
                            true);
                    result = instance.exportToCSV();
                }
                default -> logger.info("Invalid report Type");
            }

            long end = System.currentTimeMillis();
            if (result) {
                logger.info("Export jasper report succeed with time : " + (end - start) + "ms");
            } else {
                logger.error("Export jasper report has failed !!! ");
            }
        }
    }

    private Map<String, Object> createParams(String reportDate) {
        final Map<String, Object> params = new HashMap<>();
        params.put("parameter1", "Ninh Ninh");
        params.put("parameter2", "Kute");
        params.put("parameter3", "1991");
        params.put("reportTitle", "PRODUCT-REPORT");
        params.put("reportDate", reportDate);
        return params;
    }

    //C2 : tạo file .jasper  = cách Lấy path file .jrxml từ ngay trong project
    private String getTemplatesPathFromProject() {
        return JasperReportUtils.getTemplatePath(ABSOLUTE_PATH, JASPER_REPORT_FILE_NAME);
    }

    private String createTemplateFilePath(String templateFilePath) {
        return templateFilePath + JasperReportConstant.REPORT_TEMPLATE_CD.D_RPT_PRODUCT;
    }

    private String createOutputFilePath(String reportDate) {
        return config.getExportFilepath()
                + File.separator
                + JasperReportConstant.FOLDER_REPORT_CONFIG.D_RPT_PRODUCT
                + File.separator
                + reportDate;
    }

    private String createExportFilePath(String outputFolder, String reportDate) {
        return outputFolder + File.separator + makeFileName(reportDate);
    }

    private String makeFileName(String reportDate) {
        return REPORT_PREFIX + reportDate;
    }


}
