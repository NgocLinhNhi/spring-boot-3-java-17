package hell.larva.spring.batch.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "PRODUCT")
@NamedStoredProcedureQueries({
        @NamedStoredProcedureQuery(name = "getAllProducts", procedureName = "GET_ALL_PRODUCTS", resultClasses = Product.class)})
@Getter
@Setter
public class Product implements Serializable {
    private static final long serialVersionUID = -5538416125326898258L;

    @Id
    @Column(name = "SEQ_PRO")
    private Long seqPro;

    // name = column name in table Product ----> not name of table parent category
    @ManyToOne
    @JoinColumn(name = "CATEGORY_ID", nullable = false)
    @JsonBackReference
    private Category category;

    @Column(name = "PRODUCT_NAME")
    private String productName;

    @Column(name = "PRICE")
    private BigDecimal price;

    @Column(name = "IMAGE_URL")
    private String imageProduct;

    @Column(name = "NUMBER_SALES")
    private BigDecimal numberSales; // de gia tri la Long hay long ,int thi khi bi null se die dataTable

    @Column(name = "GUARANTEE")
    private BigDecimal guarantee;

    @Temporal(TemporalType.DATE)
    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Temporal(TemporalType.DATE)
    @Column(name = "UPDATE_DATE")
    private Date updateDate;

    @Column(name = "SYS_STATUS")
    private Long sysStatus;

    // mapped o day la ten bien o trong object(class java) con DetailBill
    // cho lazy vi nhieu 1 product nhieu bill -> cham
    @OneToMany(mappedBy = "product")
    private Set<DetailBill> detailBill;

    // Để join column trong table và manyToOne noó ko set được data cho object thì
    // chơi kiểu
    // tạo 1 object riêng ko map voi table trong DB
    // private transient Category categoryTest;

    private transient Long categoryId;
    private transient String categoryName;

    public Product() {
        super();
    }

    public Product(Long seqPro, Category category, String productName, BigDecimal price, String imageProduct,
                   BigDecimal numberSales, BigDecimal guarantee, Date createDate, Date updateDate, Long sysStatus,
                   Set<DetailBill> detailBill) {
        super();
        this.seqPro = seqPro;
        this.category = category;
        this.productName = productName;
        this.price = price;
        this.imageProduct = imageProduct;
        this.numberSales = numberSales;
        this.guarantee = guarantee;
        this.createDate = createDate;
        this.updateDate = updateDate;
        this.sysStatus = sysStatus;
        this.detailBill = detailBill;
    }

}