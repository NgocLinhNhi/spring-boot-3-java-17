package hell.larva.spring.batch.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import jakarta.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "BILL")
@Getter
@Setter
public class Bill implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "SEQ_BILL", nullable = false)
    private Long seqBill;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CUSTOMER_ID", nullable = false)
    @Fetch(FetchMode.SELECT)
    private User customer;

    @Column(name = "PHONE_CONTACT", nullable = false)
    private String phoneContact;

    @Temporal(TemporalType.DATE)
    @Column(name = "ORDER_DATE")
    private Date orderDate;

    @Temporal(TemporalType.DATE)
    @Column(name = "ACCEPT_DATE")
    private Date acceptDate;

    @Column(name = "SYS_STATUS_BILL")
    private int sysStatusBill;

    @Column(name = "ADDRESS_CUSTOMER")
    private String addressCustomer;

    @Column(name = "NAME_CUSTOMER")
    private String nameCustomer;

    @Column(name = "TOTAL_MONEY")
    private BigDecimal totalMoney;

    @OneToMany(mappedBy = "bill", fetch = FetchType.LAZY)
    @Fetch(FetchMode.SELECT)
    private Set<DetailBill> detailBill;

    public Bill(Long seqBill, User customer, String phoneContact, Date orderDate, Date acceptDate, int sysStatusBill,
                String addressCustomer, String nameCustomer, BigDecimal totalMoney, Set<DetailBill> detailBill) {
        super();
        this.seqBill = seqBill;
        this.customer = customer;
        this.phoneContact = phoneContact;
        this.orderDate = orderDate;
        this.acceptDate = acceptDate;
        this.sysStatusBill = sysStatusBill;
        this.addressCustomer = addressCustomer;
        this.nameCustomer = nameCustomer;
        this.totalMoney = totalMoney;
        this.detailBill = detailBill;
    }

    public Bill() {
        super();
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

}
