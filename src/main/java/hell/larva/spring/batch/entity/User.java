package hell.larva.spring.batch.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import jakarta.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "USER_CUSTOMER")
@Getter
@Setter
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "SEQ_USER", nullable = false)
    private Long seqUser;

    @Column(name = "LOGIN_NAME", nullable = false)
    private String loginName;

    @Column(name = "PASSWORD", nullable = false)
    private String password;

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "PHONE")
    private String phone;

    @Column(name = "ADDRESS")
    private String address;

    @Column(name = "GENDER")
    private int gender;

    @Column(name = "USER_NAME")
    private String userName;

    @Column(name = "ROLE_USER")
    private int roleUser;

    @Column(name = "IS_DELETE")
    private int isDelete;

    @Column(name = "IDENTITY_CARD", length = 9)
    private String identityCard;

    @Temporal(TemporalType.DATE)
    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "LINK_BILL")
    private String linkBill;

    @OneToMany(mappedBy = "customer", fetch = FetchType.LAZY)
    @Fetch(FetchMode.SELECT)
    private Set<IdeaCustomer> ideacustomers;

    @OneToMany(mappedBy = "customer", fetch = FetchType.LAZY)
    @Fetch(FetchMode.SELECT)
    private Set<Bill> bills;

    public User(Long seqUser, String loginName, String password, String email, String phone, String address, int gender,
                String userName, int roleUser, int isDelete, String identityCard, Date createDate, String linkBill,
                Set<IdeaCustomer> ideacustomers, Set<Bill> bills) {
        super();
        this.seqUser = seqUser;
        this.loginName = loginName;
        this.password = password;
        this.email = email;
        this.phone = phone;
        this.address = address;
        this.gender = gender;
        this.userName = userName;
        this.roleUser = roleUser;
        this.isDelete = isDelete;
        this.identityCard = identityCard;
        this.createDate = createDate;
        this.linkBill = linkBill;
        this.ideacustomers = ideacustomers;
        this.bills = bills;
    }

    public User() {
        super();
    }

}
