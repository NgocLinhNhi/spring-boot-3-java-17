package hell.larva.spring.batch.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;

import jakarta.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "CATEGORY")
@Getter
@Setter
public class Category implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "CATEGORY_ID", nullable = false)
    private Long categoryId;

    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "PRODUCER_ID")
    private Producer producer;

    @Column(name = "CATEGORY_NAME")
    private String categoryName;

    @Column(name = "SYS_STATUS_CATE")
    private int sysStatusCate;

    @OneToMany(mappedBy = "category")
    @JsonManagedReference //cach khac de ko dung JsonIgrnore @JsonManagedReference+@JsonBackReference
    private Set<Product> product;

    public Category(Long categoryId, Producer producer, String categoryName, int sysStatusCate, Set<Product> product) {
        super();
        this.categoryId = categoryId;
        this.producer = producer;
        this.categoryName = categoryName;
        this.sysStatusCate = sysStatusCate;
        this.product = product;
    }

    public Category() {
        super();
    }

}
