package hell.larva.spring.batch.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import jakarta.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "PRODUCER")
@Getter
@Setter
public class Producer implements Serializable {

    private static final long serialVersionUID = -4988499961184910623L;

    @Id
    @Column(name = "SEQ_NO")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int seqNo;

    @Column(name = "PRODUCER_NAME")
    private String producerName;

    @Column(name = "SYS_STATUS")
    private int sysStatus;

    @OneToMany(mappedBy = "producer", fetch = FetchType.LAZY)
    @Fetch(FetchMode.SELECT)
    private Set<Category> category;

    public Producer() {
        super();
    }

    public Producer(int seqNo, String producerName, int sysStatus, Set<Category> category) {
        super();
        this.seqNo = seqNo;
        this.producerName = producerName;
        this.sysStatus = sysStatus;
        this.category = category;
    }

}
