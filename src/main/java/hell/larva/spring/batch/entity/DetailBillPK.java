package hell.larva.spring.batch.entity;

import jakarta.persistence.Basic;
import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Embeddable
@Getter
@Setter
public class DetailBillPK implements Serializable {
    private static final long serialVersionUID = -1796781518509008144L;

    @Basic(optional = false)
    @Column(name = "BILL_ID") // ten cot trong bang DetailBill ko phai tu dat vo van nhe
    private int billId;

    @Basic(optional = false)
    @Column(name = "PRODUCT_ID")
    private int productId;

    public DetailBillPK() {
        super();
    }

    public DetailBillPK(int billId, int productId) {
        super();
        this.billId = billId;
        this.productId = productId;
    }

    public boolean equals(Object other) {
        if ((this == other))
            return true;
        if ((other == null))
            return false;
        if (!(other instanceof DetailBillPK))
            return false;
        DetailBillPK castOther = (DetailBillPK) other;

        return (this.getBillId() == castOther.getBillId()) && (this.getProductId() == castOther.getProductId());
    }

    public int hashCode() {
        int result = 17;

        result = 37 * result + this.getBillId();
        result = 37 * result + this.getProductId();
        return result;
    }
}
