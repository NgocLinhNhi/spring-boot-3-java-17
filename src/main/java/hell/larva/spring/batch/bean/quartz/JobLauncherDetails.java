package hell.larva.spring.batch.bean.quartz;

import lombok.Getter;
import lombok.Setter;
import org.quartz.JobExecutionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.configuration.JobLocator;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

import java.util.Date;
import java.util.Map;
import java.util.Map.Entry;

@Getter
@Setter
public class JobLauncherDetails extends QuartzJobBean {
    private static final Logger logger = LoggerFactory.getLogger(JobLauncherDetails.class);
    static final String JOB_NAME = "jobName";
    private JobLocator jobLocator;
    private JobLauncher jobLauncher;
    protected void executeInternal(JobExecutionContext context) {

        Map<String, Object> jobDataMap = context.getMergedJobDataMap();
        String jobName = (String) jobDataMap.get(JOB_NAME);
        JobParameters jobParameters = getJobParametersFromJobMap(jobDataMap);

        try {
            jobLauncher.run(jobLocator.getJob(jobName), jobParameters);
        } catch (Exception e) {
            logger.error("Failed to run job: ", e);
        } finally {
            stopAmsBatchJob(jobDataMap, jobName);
        }
    }

    //destroy job cho lần call thứ 2 không job đó sẽ bị chạy 2 lần gây log exception
    private void stopAmsBatchJob(Map<String, Object> jobDataMap, String jobName) {
        try {
            if (jobName.equals("schedulerAllJobs")) {
                SchedulerFactoryBean factory = (SchedulerFactoryBean) jobDataMap.get("factory");
                if (factory != null) {
                    logger.info("amsBatchSchedule triggered, this should run only once and stop - cancelling schedule ...");
                    factory.destroy();
                    logger.info("amsBatchSchedule has been cancelled");
                }
            }
        } catch (Exception e) {
            logger.error("Failed to stop amsBatchSchedule", e);
        }
    }

    private JobParameters getJobParametersFromJobMap(Map<String, Object> jobDataMap) {

        JobParametersBuilder builder = new JobParametersBuilder();

        for (Entry<String, Object> entry : jobDataMap.entrySet()) {
            String key = entry.getKey();
            Object value = entry.getValue();
            if (value instanceof String && !key.equals(JOB_NAME)) {
                builder.addString(key, (String) value);
            } else if (value instanceof Float || value instanceof Double) {
                builder.addDouble(key, ((Number) value).doubleValue());
            } else if (value instanceof Integer || value instanceof Long) {
                builder.addLong(key, ((Number) value).longValue());
            } else if (value instanceof Date) {
                builder.addDate(key, (Date) value);
            }
        }

        builder.addDate("run date", new Date());
        return builder.toJobParameters();
    }

}