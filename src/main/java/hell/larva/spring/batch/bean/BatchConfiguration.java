package hell.larva.spring.batch.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BatchConfiguration {

    private String reportDate;
    private String dateFormat;
    private static BatchConfiguration instance;

    //Tạo instance singleTon bình thường => thì sao lấy được value trong Bean xml => nên phải tạo bean static kiểu này .
    public static synchronized BatchConfiguration getBatchConfiguration() {
        if (instance == null) {
            instance = new BatchConfiguration();
        }
        return instance;
    }

    //field là dạng static thì phải set value kiểu này => mới ăn được vào Bean
    //hoặc @Setter ngay biến static
    public static void setInstance(BatchConfiguration instance) {
        BatchConfiguration.instance = instance;
    }
}
