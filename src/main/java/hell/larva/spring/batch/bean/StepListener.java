package hell.larva.spring.batch.bean;


import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;

public class StepListener implements StepExecutionListener {

    @Override
    public void beforeStep(StepExecution stepExecution) {
    }

    @Override
    public ExitStatus afterStep(StepExecution stepExecution) {
        if (stepExecution.getStatus() == BatchStatus.COMPLETED) {
            System.out.println("Step {} run complete." + stepExecution.getStepName());
            return ExitStatus.COMPLETED;
        } else if (stepExecution.getStatus() == BatchStatus.FAILED) {
            System.out.println("Step {} run fail." + stepExecution.getStepName());
            return ExitStatus.FAILED;
        } else if (stepExecution.getStatus() == BatchStatus.UNKNOWN) {
            System.out.println("Step {} return unknown result." + stepExecution.getStepName());
            return ExitStatus.UNKNOWN;
        }
        return null;
    }

}
