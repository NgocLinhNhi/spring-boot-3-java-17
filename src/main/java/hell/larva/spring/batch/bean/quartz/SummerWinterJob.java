package hell.larva.spring.batch.bean.quartz;

import hell.larva.spring.batch.startup.BatchServerTelnetStartUp;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

import java.text.ParseException;

public class SummerWinterJob extends QuartzJobBean {

    private final Logger logger = LoggerFactory.getLogger(SummerWinterJob.class);

    @Override
    protected void executeInternal(JobExecutionContext context) {
        try {
            logger.info("Executing SummerWinterJob ...");
            ApplicationContext appContext = BatchServerTelnetStartUp.getAppContext();
            scheduleOpenMarketJobs(appContext);
        } catch (Exception e) {
            logger.error("Failed to start amsBatchSchedule", e);
        }
    }

    private void scheduleOpenMarketJobs(ApplicationContext appContext) throws Exception {
        JobDetail jobDetail = (JobDetail) appContext.getBean("amsBatchSchedule");
        SchedulerFactoryBean schedulerFactoryBean = new SchedulerFactoryBean();
        jobDetail.getJobDataMap().put("factory", schedulerFactoryBean);

        //Giờ phút giây này có thể lấy từ 1 bảng config nào đó trong DB
        //Để set up động giờ khởi động cho Batch- Theo mùa !!!
        //Hoặc là có thể lấy từ Bean trong application.properties (Như BO lúc đầu)!
        int hours = 21;
        int minutes = 9;
        int seconds = 0;

        CronTriggerFactoryBean cronTriggerFactoryBean = buildCronTriggerFactoryBean(
                hours,
                minutes,
                seconds,
                jobDetail);

        SchedulerFactoryBean schedulerFactory = buildSchedulerFactory(
                cronTriggerFactoryBean,
                jobDetail,
                schedulerFactoryBean);

        schedulerFactory.start();
    }

    //Set up giờ chạy hàng ngày cho Batch
    private CronTriggerFactoryBean buildCronTriggerFactoryBean(
            int hours,
            int minutes,
            int seconds,
            JobDetail jobDetail) throws ParseException {

        CronTriggerFactoryBean cronTriggerFactoryBean = new CronTriggerFactoryBean();
        cronTriggerFactoryBean.setJobDetail(jobDetail);
        String cronExpression = String.format(
                "%s %s %s ? * MON-SUN *",
                seconds,
                minutes,
                hours);
        System.out.println("cronExpression " + cronExpression);
        cronTriggerFactoryBean.setCronExpression(cronExpression);
        cronTriggerFactoryBean.afterPropertiesSet();

        logger.info("Starting openMarketJobs: cronExpression={}", cronExpression);
        return cronTriggerFactoryBean;
    }

    private SchedulerFactoryBean buildSchedulerFactory(CronTriggerFactoryBean cronTriggerFactoryBean,
                                                       JobDetail jobDetail,
                                                       SchedulerFactoryBean schedulerFactoryBean) throws Exception {
        schedulerFactoryBean.setTriggers(cronTriggerFactoryBean.getObject());
        schedulerFactoryBean.setJobDetails(jobDetail);
        schedulerFactoryBean.afterPropertiesSet();
        return schedulerFactoryBean;
    }

}
