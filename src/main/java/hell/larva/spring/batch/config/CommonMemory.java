package hell.larva.spring.batch.config;

import hell.larva.spring.batch.constant.JasperReportConstant;

import java.util.HashMap;
import java.util.Map;

public class CommonMemory {
    private static CommonMemory instance;
    private static final Map<String, Boolean> batchStatus = new HashMap<>();
    private static Map<String, String> batchResult = new HashMap<>();

    private static final String NOT_YET_START = "NOT YET START";
    private static final String STARTED_SUCCESSFULLY = "STARTED SUCCESSFULLY";
    private static final String STARTED_FAIL = "STARTED FAIL";

    public static CommonMemory getInstance() {
        if (instance == null) {
            instance = new CommonMemory();
            initBatchResult();
        }
        return instance;
    }

    public static void clearMem() {
        instance = null;
    }

    private static void initBatchResult() {

        batchResult = new HashMap<>();
        batchResult.put(JasperReportConstant.SUMMARY_TYPE.INIT_TASKLET, NOT_YET_START);

        batchResult.put(JasperReportConstant.SUMMARY_TYPE.UPDATE_PRODUCER_TASKLET, NOT_YET_START);
        batchResult.put(JasperReportConstant.REPORT_TEMPLATE_CD.D_RPT_PRODUCT, NOT_YET_START);
        batchResult.put(JasperReportConstant.REPORT_TEMPLATE_CD.M_RPT_PRODUCT, NOT_YET_START);
    }

    public void updateBatchStatus(String reportName, Boolean reportStatus, Exception e) {
        batchStatus.put(reportName, reportStatus);
        batchResult.put(reportName, reportStatus ? STARTED_SUCCESSFULLY : (STARTED_FAIL + " ===> " + e.getMessage()));
    }

    public Map<String, Boolean> getBatchStatus() {
        return batchStatus;
    }

    public Map<String, String> getBatchResult() {
        return batchResult;
    }
}
