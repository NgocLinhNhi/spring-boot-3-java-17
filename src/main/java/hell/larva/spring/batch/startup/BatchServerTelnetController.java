package hell.larva.spring.batch.startup;

import com.google.common.base.MoreObjects;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.primitives.Ints;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.*;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.context.ApplicationContext;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class BatchServerTelnetController extends Thread {
    private static final Logger logger = LoggerFactory.getLogger(BatchServerTelnetController.class);

    private final ApplicationContext context;

    BatchServerTelnetController(ApplicationContext context) {
        this.context = context;
        this.setName("batch-telnet-socket");
    }

    private void userDefineProcessing(String command, final PrintWriter out) {
        try {
            if (Strings.isNullOrEmpty(command)) {
                return;
            }
            if ("ALL".equalsIgnoreCase(command)) {
                @SuppressWarnings("unchecked")
                List<Job> jobList = (List<Job>) context.getBean("jobList");
                for (Job job : jobList) {
                    try {
                        JobLauncher jobLauncher = (JobLauncher) context.getBean("jobLauncher");
                        Map<String, JobParameter> jobParameter = new HashMap<>();
                        jobParameter.put("run_date", new JobParameter(new Date()));
                        JobExecution execution = jobLauncher.run(job, new JobParameters(jobParameter));
                        while (execution.getStatus().isRunning()) {
                            Thread.sleep(1000);
                        }
                        if (execution.getStatus() != BatchStatus.COMPLETED) {
                            throw new Exception("job " + job.getName() + " return " + execution.getStatus() + " Stop all job");
                        }
                    } catch (Exception e) {
                        logger.error("can't run job : ", e);
                        break;
                    }
                }
            } else {
                // command is job id or job chain
                // Constant->B,C->D
                // [Constant],[B,C],[D]
                List<String> jobSplit = Splitter.on("->").splitToList(command);
                List<List<String>> jobCommand = jobSplit.stream().map(input -> {
                    List<String> returnList;
                    if (Strings.isNullOrEmpty(input)) {
                        returnList = Lists.newArrayList();
                    } else {
                        returnList = Splitter.on(",").splitToList(input);
                    }
                    return returnList;
                }).collect(Collectors.toList());

                out.println("\nJob will be process: " + jobCommand + "\n");

                JobLauncher jobLauncher = (JobLauncher) context.getBean("jobLauncher");
                List<JobExecution> jobExecutions;
                boolean isRunning;
                for (List<String> listJob : jobCommand) {
                    jobExecutions = Lists.newArrayList();
                    for (String jobSimulate : listJob) {
                        Job job = (Job) context.getBean(jobSimulate);
                        Map<String, JobParameter> jobParameter = new HashMap<>();
                        jobParameter.put("run_date", new JobParameter(new Date()));
                        JobExecution execution = jobLauncher.run(job, new JobParameters(jobParameter));
                        jobExecutions.add(execution);
                    }
                    while (true) {
                        isRunning = jobExecutions.stream().anyMatch(input -> {
                            if (!input.getStatus().isRunning())
                                out.println("Completed: " + input.getJobInstance().getJobName());
                            return input.getStatus().isRunning();
                        });
                        if (isRunning) {
                            Thread.sleep(1000);
                        } else {
                            break;
                        }
                    }
                }

                out.println("Completed all jobs: " + jobCommand);
            }
        } catch (Exception e) {
            logger.error("Processing run job has failed: ", e);
        }
    }

    @Override
    public void run() {
        try {
            String configPort = System.getProperty("serverSocketPort");
            if (Strings.isNullOrEmpty(configPort)) {
                if (context.containsBean("serverSocketPort")) {
                    configPort = (String) context.getBean("serverSocketPort");
                }
            }

            int port = Ints.tryParse(MoreObjects.firstNonNull(configPort, "1605"));
            ServerSocket myserver = null;
            Socket client = null;
            PrintWriter out = null;
            BufferedReader in = null;
            String inputLine;

            while (true) {
                try {
                    myserver = new ServerSocket(port);
                    logger.info("Listening on port {}", port);
                    client = myserver.accept();
                    out = new PrintWriter(client.getOutputStream(), true);
                    in = new BufferedReader(new InputStreamReader(client.getInputStream()));

                    @SuppressWarnings("unchecked")
                    List<Job> jobList = (List<Job>) context.getBean("jobList");

                    out.println("All job id: ");
                    for (Job job : jobList) {
                        out.println("\t" + job.getName());
                    }

                    out.println("Please write job id or job chain to run, or type ALL for chain all job like display order");
                    out.println("Example: Constant->B,C->D means that job Constant run complete then job B and C will run simulatively, after that job D will run");
                    out.println("If one of this job in chain fail, all fail.");

                    while ((inputLine = in.readLine()) != null) {
                        // inputLine is a command from cmd
                        userDefineProcessing(inputLine, out);
                    }
                } finally {
                    if (out != null) out.close();
                    if (in != null) in.close();
                    if (client != null) client.close();
                    if (myserver != null) myserver.close();
                }

            }
        } catch (Exception e) {
            logger.error("Run job Batch has failed: ", e);
        }
    }
}