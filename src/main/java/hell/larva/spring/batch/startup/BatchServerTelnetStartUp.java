package hell.larva.spring.batch.startup;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameter;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class BatchServerTelnetStartUp {
    private static ApplicationContext appContext;
    private static final Logger logger = LoggerFactory.getLogger(BatchServerTelnetStartUp.class);

    public static void main(String[] args) {
        try {
            //Run Batch with telnet
            //Start bình thường thì nó cũng sẽ run được job config cứng bình thường như BatchStartUp luôn
            logger.info("Starting Batch System... ... ...");
            String springConfig = "batch-jobs.xml";
            appContext = new ClassPathXmlApplicationContext(springConfig);

            BatchServerTelnetController serverSocketCtrl = new BatchServerTelnetController(appContext);
            serverSocketCtrl.start();

            logger.info("\nSTART Batch SUCCESS\n");
        } catch (Exception e) {
            logger.error("START Batch has failed", e);
            throw new IllegalStateException(e);
        }
    }

    public static void executeJob(ApplicationContext appContext, String jobName) {
        logger.info("Begin running {} ... ... ...", jobName);
        try {
            JobLauncher jobLauncher = (JobLauncher) appContext.getBean("jobLauncher");
            Map<String, JobParameter> jobParameter = new HashMap<>();
            jobParameter.put("runDate", new JobParameter(new Date()));

            Job jobMain = (Job) appContext.getBean(jobName);

            jobLauncher.run(jobMain, new JobParameters(jobParameter));

        } catch (Exception e) {
            logger.error("Run job Batch has exception: ", e);
        }
    }

    public static ApplicationContext getAppContext() {
        return appContext;
    }
}
