package hell.larva.spring.batch.startup;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class BatchStartUp {
    public static void main(String[] args) {
        //Run batch bình thường không có telnet - Basic learn from network
        BatchStartUp obj = new BatchStartUp();
        obj.runBatchTask();
    }

    private void runBatchTask() {
        //load the batch config file
        String[] batchConfig = {"batch-jobs.xml"};
        ApplicationContext context = new ClassPathXmlApplicationContext(batchConfig);
        JobLauncher jobLauncher = (JobLauncher) context.getBean("jobLauncher");

        //load the jobs bean
        //schedulerAllJobsFlow = định nghĩa nhiều job thread chạy song song
        //schedulerAllJobs = 1 Job tổng chạy job lần lượt
        Job job = (Job) context.getBean("schedulerAllJobs");
        try {
            JobExecution execution = jobLauncher.run(job, new JobParameters());
            System.out.println("Job Exit Status : " + execution.getStatus());
        } catch (Exception e) {
            System.out.println("BATCH RUN HAS ERROR");
            e.printStackTrace();
        }
        System.out.println("BATCH RUN FINISHED");
    }
}
