package hell.larva.spring.batch.service;

import hell.larva.spring.batch.dao.ProductDaoImpl;
import hell.larva.spring.batch.entity.Producer;
import hell.larva.spring.batch.tasklet.ProducerTasklet;
import hell.larva.spring.batch.utils.csv.SimpleCsvPrinter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.List;

import static hell.larva.spring.batch.constant.Constant.*;

public final class ProductService implements IProductService {
    private final Logger logger = LoggerFactory.getLogger(ProducerTasklet.class);

    private static final String CSV_EXPORT_FILE_NAME = "producer.csv";
    private static final String CSV_SEPARATE = ",";
    private final ProductDaoImpl productDao;
    public ProductService(ProductDaoImpl productDao) {
        this.productDao = productDao;
    }

    @Override
    public List<Producer> getListProducer() {
        logger.info("START Load Producers");
        List<Producer> producers = productDao.loadAllProducer();
        producers.forEach(producer -> System.out.println(producer.getProducerName()));
        logger.info("END Load Producers");
        return producers;
    }

    @Override
    public void exportToFile(List<Producer> producers,
                             String reportDate,
                             String absolutePath) {
        logger.info("Start Export Producer to csv file ");
        exportCsvFile(producers, reportDate, absolutePath);
        logger.info("End Export Producer to csv file ");
    }

    private void exportCsvFile(List<Producer> producers,
                               String reportDate,
                               String absolutePath) {
        try (SimpleCsvPrinter printer = new SimpleCsvPrinter(getCsvFileName(absolutePath))) {
            printer.write(buildHeader().toString());
            for (Producer producer : producers) {
                printer.write(
                        reportDate,
                        String.valueOf(producer.getSeqNo()),
                        producer.getProducerName(),
                        producer.getSysStatus() == 1 ? ACTIVE : UN_ACTIVE
                );
            }
            printer.flush();
            producers.clear();
        }
    }

    private StringBuilder buildHeader() {
        StringBuilder buildHeader = new StringBuilder();

        buildHeader.append(DATE);
        buildHeader.append(CSV_SEPARATE);
        buildHeader.append(SEQ);
        buildHeader.append(CSV_SEPARATE);
        buildHeader.append(PRODUCER_NAME);
        buildHeader.append(CSV_SEPARATE);
        buildHeader.append(STATUS);

        return buildHeader;
    }

    private String getCsvFileName(String absolutePath) {
        String filePath = absolutePath + File.separator + CSV_EXPORT_FILE_NAME;
        logger.info("File Path on server : " + filePath);
        return filePath;
    }
}
