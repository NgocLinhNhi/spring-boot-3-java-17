package hell.larva.spring.batch.service;

import hell.larva.spring.batch.config.CommonMemory;
import hell.larva.spring.batch.tasklet.ProducerTasklet;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.Set;

public class BatchRunResult {
    private static final Logger logger = Logger.getLogger(ProducerTasklet.class);

    private static final String BATCH_RUN_SUCCESSFULLY = "BATCH RUN SUCCESSFULLY";
    private static final String BATCH_RUN_FAIL = "BATCH RUN FAIL";

    private final CommonMemory commonMemory;
    private boolean result;

    public static BatchRunResult getInstance() {
        return new BatchRunResult();
    }

    private BatchRunResult() {
        this.commonMemory = CommonMemory.getInstance();
    }

    public void checkRunBatchResult() {
        Map<String, Boolean> status = commonMemory.getBatchStatus();
        logger.info("Checking running results...");

        checkingResultAllBatch(status);
        StringBuilder resultBatchRun = createResultMessageRunBatch();
        getDetailResultRunBatch(resultBatchRun);
    }

    private void getDetailResultRunBatch(StringBuilder resultBatchRun) {
        Map<String, String> result = commonMemory.getBatchResult();
        Set<String> keySet = result.keySet();
        ArrayList<String> list = new ArrayList<>(keySet);
        Collections.sort(list);

        for (String batchName : list) {
            logger.info("Batch " + batchName + " : " + result.get(batchName));
            resultBatchRun.append("\r\n");
            resultBatchRun.append("Batch ");
            resultBatchRun.append(batchName);
            resultBatchRun.append(": ");
            resultBatchRun.append(result.get(batchName));
        }

        //Display result run batch
        String batchResult = resultBatchRun.toString();
        System.out.println("Result run Batch : " + batchResult);
    }

    private void checkingResultAllBatch(Map<String, Boolean> status) {
        for (String report : status.keySet()) {
            if (!status.get(report)) {
                this.result = false;
                break;
            }
        }
    }

    private StringBuilder createResultMessageRunBatch() {
        StringBuilder resultBatchRun = new StringBuilder();

        if (result) {
            resultBatchRun.append(BATCH_RUN_SUCCESSFULLY);
        } else {
            resultBatchRun.append(BATCH_RUN_FAIL);
        }
        resultBatchRun.append(" \r\n");

        logger.info("Result run Batch : " + resultBatchRun);
        return resultBatchRun;
    }

}
