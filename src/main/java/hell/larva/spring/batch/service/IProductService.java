package hell.larva.spring.batch.service;

import hell.larva.spring.batch.entity.Producer;

import java.util.List;

//Java 17 sealed interface  class - chỉ cung cấp quyền kế thừa cho ProductService class
public sealed interface IProductService permits ProductService {

    List<Producer> getListProducer();

    void exportToFile(List<Producer> producers, String reportDate, String absolutePath);

    //Java 8 Interface default method - method non abstract có thể xử lý trong này
    default void awake(List<Producer> listProducer) {
        doSomething(listProducer);
    }

    default void sleep(List<Producer> listProducer) {
        doSomething(listProducer);
    }

    //Java 9 Interface private method
    private static void doSomething(List<Producer> listProducer) {
        for (Producer producer : listProducer) {
            producer.setSysStatus(2);
        }
        System.out.println("This is Java 9 interface private method");
    }
}
