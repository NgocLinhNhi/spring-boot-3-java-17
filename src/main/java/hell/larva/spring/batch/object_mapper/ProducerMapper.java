package hell.larva.spring.batch.object_mapper;

import hell.larva.spring.batch.entity.Producer;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ProducerMapper implements RowMapper<Producer> {

    @Override
    public Producer mapRow(ResultSet rs, int rowNum) throws SQLException {
        Producer pro = new Producer();
        pro.setSeqNo(rs.getInt("seq_no"));
        pro.setProducerName(rs.getString("producer_name"));
        pro.setSysStatus(rs.getInt("sys_status"));
        return pro;
    }

}