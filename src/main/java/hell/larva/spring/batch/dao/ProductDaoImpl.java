package hell.larva.spring.batch.dao;

import hell.larva.spring.batch.entity.Producer;
import hell.larva.spring.batch.entity.Product;
import hell.larva.spring.batch.interfaces.IProductDao;
import hell.larva.spring.batch.object_mapper.ProducerMapper;
import hell.larva.spring.batch.object_mapper.ProductMapper;
import hell.larva.spring.batch.sql.SqlQuery;
import lombok.Getter;
import lombok.Setter;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.util.List;

@Getter
@Setter
public class ProductDaoImpl implements IProductDao {

    private DataSource dataSource;

    @Override
    public List<Producer> loadAllProducer() {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        return jdbcTemplate.query(SqlQuery.PRODUCER_LOAD_ALL, new ProducerMapper());
    }

    @Override
    public List<Product> loadAllProduct() {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        return jdbcTemplate.query(SqlQuery.PRODUCT_LOAD_ALL, new ProductMapper());
    }

}
